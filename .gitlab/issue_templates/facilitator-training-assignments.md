# TeamOps Facilitator Training

Hello TeamOps Facilitator training cohort! You're nearly complete with the TeamOps facilitator training. At this point, you should have completed the [TeamOps Facilitator Training Learning Path in Level Up](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/learning-path/teamops-facilitator-training). Please prioritize the 2 short courses in this learning path before starting the assignments in this issue.

# Assignment 1: Practice Session: Mock Facilitations

Let's put your skills to the test. Simulated practice of a TeamOps workshop is a great way to build confidence and get used to speaking in a facilitator role. You have 2 options to complete this assignment.

## Option 1: (Preferred) Live practice with 1-2 other facilitators

Meet in a 25 minute session with one or two other training facilitators to practice a mock facilitation of one piece of the TeamOps workshop. Each facilitator should practice as if they are giving a live TeamOps workshop for ~5 minutes. Following this, other facilitators on the call should give feedback and discuss iterations. Here are some suggested prompts/sections of the workshop that you might try:

1. Practice designing and facilitating an icebreaker. Facilitator goals are to help each attendee feel individually seen and welcome, and to build psychological safety as a group.
1. Practice managing an open-ended discussion on any topic of your choice. Facilitator goals are to solicit equal engagement from all attendees (balancing too much from extroverts and too little from introverts), minimize "dead space" in the conversation with easy-to-answer questions, and pace the discussion with effective time management.
1. Practice running a poll and discussing the results. Facilitator goals are to practice the poll feature of zoom and create a smooth segue from a quantitative activity to a verbal activity.

Use the table below to organize and sign up for live sessions. The `organizer/facilitator 1` will pick a date and time, set up a meeting invitation, and add the information to the table below. Facilitators 2 and 3 will sign up for a session that works for them by adding their name to the table. Following the session, please return to this issue and post any questions or findings that came out of your mock facilitator.

| Session Time and Date | Organizer/Facilitator 1 | Facilitator 2 | (Optional) Facilitator 3 |
| ----- | ----- | ----- | ----- |
| ----- | ----- | ----- | ----- |
| ----- | ----- | ----- | ----- |
| ----- | ----- | ----- | ----- |
| ----- | ----- | ----- | ----- |
| ----- | ----- | ----- | ----- |
| ----- | ----- | ----- | ----- |
| ----- | ----- | ----- | ----- |
| ----- | ----- | ----- | ----- |
| ----- | ----- | ----- | ----- |
| ----- | ----- | ----- | ----- |


## Option 2: Recorded practice

We understand that your timezone and/or working hours might make it difficult to pair live with another training facilitator. If you cannot find a facilitator to pair with, you may complete the assignment by recording your mock facilitation. Please post the recording to this issue so folks can share async feedback.
