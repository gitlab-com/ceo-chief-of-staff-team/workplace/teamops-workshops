## Prepare Program Materials 

- [ ] Schedule workshop dates (noting a maximum group size of 15 participants per session) 
   - [ ] Create Calendar Invitations for each session 
- [ ] Create and customize an facilitation issue, using this issue as a template 
- [ ] Create and customize an application issue (using [this issue](https://gitlab.com/gitlab-com/ceo-chief-of-staff-team/workplace/teamops-workshops/-/issues/1) as a template) 
- [ ] Duplicate [the program outline template](https://docs.google.com/document/d/1I9DDDBKGD5NxUVXptcdofMuEN19DNbTXdrEG-q2L-8Q/edit?usp=sharing), and customize the highlighted fields as necessary (workshop dates, deadlines, etc.)
- [ ] Duplicate the workshop agenda doc, and customize the highlighted fields as necessary (time stamps, hyperlinks, etc.) 
   - [ ] Add the workshop agenda to the calendar invitation 
- [ ] Create a slack channel for each group (such as #teamops-trainer-q1-g1, #teamops-trainer-q2-g1)
- [ ] Update all hyperlinks in the program outline and application issue to accurately direct participants to this cohort's program outline, application issue, and feedback issue. 

## Recruit Participants

- [ ] Announce that the application issue is open on the #teamops channel, then share the original post on other channels for more visibility, such as #whats-happening-at-gitlab, #learning-and-development, #customer-success, #solutions-architects, and #career-enablement-tmag
   - [ ] Extend 1:1 invitations to anyone you think might benefit from participating, and encourage all TeamOps Facilitators to do the same on the #teamops-private channel

## Cohort Organization 

- [ ] At least once a week, review new comments on the application issue to accept applications and sort participants into groups (:movie_camera: [Watch a tutorial video of this process here](https://www.loom.com/share/ac92a2b955694fd291a20a14005a6e24))
   - [ ] Confirm the eligibility of each application. If complete, add a :white_check_mark: to their comment to confirm receipt. 
   - [ ] Sort the applicant into the their requested group by adding their name to the `Group Assignments` table in the issue description
   - [ ] Add the applicant to their group's calendar event (and send them an invitation)
   - [ ] Add the applicant to their group's slack channel 
   - [ ] Add a comment [like this](https://gitlab.com/gitlab-com/ceo-chief-of-staff-team/cos-team/-/issues/305#note_1203830849) to the issue to notify applicants of their acceptance into the program (and mark the end of that "batch" of applications that have been processed)
- [ ] When a cohort fills up, change the relevant line for that group from a checkbox to a "CLOSED:" in both the `Program Timeline` and `Application and Proof of Eligibility` sections of the application issue.
- [ ] Coordinate requests to change groups as needed

## Program Facilitation

- [ ] `Thursday before the week of the program` **Send a welcome message to each group via Slack**
   - _Template:_ Welcome to Group 1's Slack channel for the TeamOps Trainer Pilot Program! My name is _____. I'll be your facilitator during the program next week. We'll primarily use this channel for learning experience support and continuing the conversations that we start during the workshop. No action needed here (or anywhere) until the program kicks off on Monday. But in the meantime, if you have any questions or just want to share your excitement with your group members, this is a great place to start those discussions.
- [ ] `Monday, week of the program` **Send a program kickoff message to each group via Slack** 
   - _Template:_ Today is TeamOps Trainer day! The program outline (link) is now live and ready for your action. (It's also stored in the application issue for future reference.) This shared google doc will be our Single Source of Truth (SSoT) during the program, and the guide for your learning path. All links, resources, and instructions will be in, or lead back to, this "headquarter" document. To start the program, just access the document and follow the instructions. Note that all pre-workshop assignments should be complete before your workshop. Those take about 3 hours in total, so you've got plenty of time!
   - _Tip:_ Write this message at the same time that you write the welcome message, but schedule it to arrive on Sunday evening, so that it's in everyone's inbox on Monday morning, regardless of their time zone. 
- [ ] `Tuesday or Wednesday, week of the program` **Send a reminder about the workshop**
   - _Template:_ Reminder: The TeamOps Trainer Workshop for Group 2 is starting at ____ tomorrow! We're going to have a lot of time together, so I suggest blocking about 15-30 minutes on your calendar prior to the workshop to take a bio break, stretch your body, grab a favorite beverage, and get comfy. I'll see you soon!
- [ ] `Wednesday` **Facilitate the workshop**
   - Follow your training and the workshop agenda to deliver a great synchronous learning experience! 
   - :movie_camera: Watch an example of the TeamOps Trainer workshop in action here: 
      - Part 1: [Recording Link](https://gitlab.zoom.us/rec/play/zhysiXAgw_WYgSYrz5E6Xd7qezM7PGjfCjN8EW9Nh9LXM8Npt_-rrqxBJ8YmdcHn6X6B4PMzbNHa1wI9.6BvI6TbBnBryXe2K) Passcode: *.^t6NQA
      - Part 2: [Recording Link](https://gitlab.zoom.us/rec/play/AApmDZPHTQDtZWQaXGO9ug_Hv4-y4Caqkhltsgo3ln78NP0oM-siIlLmBeWwUyfDtF6G6yE3nh_bNrCM.74xKpH-fXl8KmvLu) Passcode: *.^t6NQA
- [ ] `Wednesday, right after each workshop` **Post a message of gratitude and motivation on Slack.**
   - _Template:_ Thank you all for attending the workshop! It was so fun to get to know each of you a little better, and hear your AMAZING ideas for TeamOps teaching strategies! Remember, next steps are to complete your post-workshop assignments. Refer to the SSoT (link) for instructions. And here's a plot twist: if you want to collaborate with any of your group members to produce a piece of content, you can!
- [ ] `Friday, week of program` **Post a conclusion message on Slack**
   - _Template:_ Happy Friday, Group 4! You did it! You're almost a graduate of the TeamOps Trainer Program! Thank you again and again for your participation and contributions. It's sincerely been a pleasure connecting with you. I'm grateful for you individually and collectively for making it such a success. Now that our time as a group has finished, I'm going to close this channel tomorrow. Please complete all of your post-workshop assignments and feedback before the end of your work day on Friday in your local time zone. After you're done, please continue to follow along and support the progress of TeamOps by joining and engaging in the #teamops channel. See you there!
- [ ] `Friday, week of program` **Confirm completion of assignments and feedback by monitoring checkmarks in the application issue**
   - Send individual DMs (or tag them in a comment on the application issue) to program participants that haven't completed their assignments

## Post-Program Wrap Up 

- [ ] Close the application issue 
- [ ] Mark the program outline as "closed" or "deprecated" in the doc title
- [ ] If any participants missed their workshop, instruct them to join the #teamops channel in Slack for early notification about to the next quarter's program applications
- [ ] Review and Analyze Feedback (if desired)
- [ ] Report on program success and new learnings to your fellow facilitators in #teamops-private
